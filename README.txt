osp.work.stories
======
This is a repository gathering hd pictures and pdf of OSP works (print,web, tools) for publications and presentations.


OSP in french
------
OSP écrit et dessine en utilisant uniquement des logiciels libres et open source. C'est une caravane de praticiens provenant de divers domaines. Ils conçoivent, programment, recherchent et enseignent. Travaillant pour le print, le web et ses croisements, OSP crée des identités visuelles et des services digitaux, creusant toujours vers une relation plus intime avec l'outil.
Fondé en 2006 à Bruxelles dans le cadre de l'association Constant, Open Source Publishing rassemble 9 membres actifs. Venus de Belgique, de France et des Pays Bas ils sont installés depuis peu dans les espaces de l'ancien Hacker Space de Schaerbeek et publient l'ensemble de leurs fichiers à l'adresse suivante : osp.kitchen

OSP print party
------
Les print party sont pour eux des terrains de rencontres avec un public plus grand, des moments où la mise à nu des outils rend concrète leur démarche et pratique la discussion. Initialement construites autour de l'impression d'un feuillet ou livret, ces performances participatives étaient d'abord l'occasion de raconter la découverte de nouveaux outils libres, une récente rencontre ou recherche. Avec toujours, au final, la production d'un support imprimé sur place, rassemblant les ingrédients de la présentation, laissant le public repartir en autonome, libre de rejouer l’événement — du manuel d'imposition permettant à chacun de construire un 16 pages, aux instructions pour faire chanter son ordinateur.
Depuis le code et le papier se sont reconciliés et les print party osp s'étendent à d'autres surfaces, mais il s'agit toujours bien de fêter, de réceptionner ensemble cette tension de l'écriture à l'objet, du geste au code et de la recette à la bouche.

http://osp.kitchen/work/stories/tree/master/live/#project-detail-files


Print
------
## Atelier de Création Sonore Radiophonique ##
- acsr_ecoutes-radio_2014_1.jpg
- acsr_ecoutes-radio_2014_2.jpg
- acsr_ecoutes-radio_2014_3.jpg
- acsr_ecoutes-radio_2014_4.jpg
- acsr_ecoutes-radio_2014_verso.jpg
EN: Flyers announcing a series of radio plays *Les écoutes radiophoniques*. Tools: Gimp and Scribus. Printing: Risograph.
FR: Flyers annonçant'Les écoutes radiophoniques', série d'écoutes de documentaires et de créations sonores.
Outils: Gimp et Scribus. Printing/impression: Risograph.

http://osp.kitchen/work/stories/tree/master/Atelier_de_Creation_Sonore/#project-detail-files

	
## Théâtre de la Balsamine ##
- balsamine_poster-saison_13-14.tiff
EN: Poster announcing the new season. Tools: Scribus. Printing: Offset print, A1. 
FR: Poster de saison, 2013- 2014. 
Outils: Scribus. Impression Offset, A1.
- balsa_2013-2014_x-x.tiff
Doubles pages extraits du programme de saison 2013 / 2014.
- IMG_20130904_145952.jpg
Chaque année le programme de la saison est peint sur la façade du théâtre par le peintre Marc de Meyer. 

- balsamine-logo.svg
EN: In OSP’s identity for the Balsamine, all graphical elements of the previous logo have been disassembled, rearranged and inserted of the heel of the letter b.
FR: L'ensemble logo de la Balsamine prend comme point de départ les élements graphiques du logo précedent. Réarrangées comme un set de pièces, cette collection de modules a été insérés dans la botte de la lettre b, à l'intérieure de la police utilisée dans la mise en page.

## Seoul-font-karaoke ##
- seoul-fonts-karaoke-outlined.pdf
Flyer d'annonce pour la 'print party' *Seoul Font Karaoke*.
"En direct du Lab sin01 (espace de travail du collectif SIN), à l'intérieur de la galerie De La Charge, OSP vous invite à visiter les entrailles de Tesseract, logiciel d'OCR open source. Pendant ce temps, Fonzie sera nourrit de photos du voyage et des 파전 seront préparées (crêpes coréennes). Goût Open source karaoké."
Régulièrement OSP présente ses recherches et traces de voyages groupés sous forme de performances publiques. Seoul Font Karaoke a eu lieu le 23 janvier 2014, en écho au workshop et à l'exposition menée par OSP à Séoul à l'occasion de la biennale de la typographie, Typojanchi 2013.

## 16 case stories re-imagining the practice of lay-out ##
EN: This catalogue was put together as part of an attempt to rethink lay-out tools, and features case stories that range from historical to present, on to the imaginary and speculative. The ‘camera-ready copy’ of this book was created on a pen plotter: this has required a rethinking of the workflow, including a special font stack built upon engraving tools, to be able to use stroke based fonts. Tools: Active Archives, Libre Office, Inkscape, Custom Herschey Inkscape plugin, Roland pen plotter, laser printer, glue and scissors. Printing: Risograph and Roland Pen Plotter
FR: Catalogue rendant compte des pistes d’un coloque requestionnant les outils et pratiques de la mise en page. Sa réalisation même a nécessité de repenser le workflow de mise en page habituel. Outils: Active Archives, Libre Office, Inkscape, plugin Inkscape Herschey sur mesure, plotter Roland, imprimante laser, colle et ciseaux. Impression Risograph et plotter Roland.

## Schrbksetaal – Mots de la Cage aux Ours ##
EN: 
FR: 
La Langue Schaerbeekoise récolte les mots utilisés par les différents groupes de population autour de la Cage aux Ours. Le projet veut favoriser la cohésion sociale entre différentes communautés tout en réflectant la richesse linguistique présente dans le quartier de la Place Verboekhoven à Schaerbeek, Bruxelles. Cette publication reprend l'ensemble des rencontres et interviews menées par l'association Constant et les rassemble sous la forme d'un dictionnaire.
Outils: Scribus. Impression: Offset.


Fonts
------

## Crickx ##
OSP-CRICKX est une réinterprétation digitale de lettres adhésives coupées à la main, vendues dans un magasin à Schaerbeek à Bruxelles.

## Sans Guilt ##
Fontes issues du workshop “Read the Fucking Manual” au Royal College of Art, souhaitant la bienvenue à Eric Gill dans le domaine public. Les trois versions ont été produites respectivement à partir des dessins originaux de Gill, des caractères en plomb et des caractères numériques de Monotype, encore sous droits d’auteur. Ce travail peut être vu comme une provocation ou comme une manière d’éclaircir les questions du droit d’auteur en typographie.

## Fluxisch Else (ex Univers Else) ##
Caractère réalisé par numérisation de l’Univers utilisée dans le *Fluxus Codex* de George Maciunas, composé avec une IBM Selectric. Les aspérités de cette version imprimée ont été maintenues, la faisant échapper à la fois à l’origine moderniste de la lettre et à la stérilité supposée du numérique.

## AX-28 ##
Version de l’Ax 28 Script redessinée sur base d'une lettre d'amour trouvée dans la rue. Cette fonte était utilisée par les dactylotypes Brother. Elle est en usage pour le projet *Travelling féministe*, une plateforme développée avec le Centre Simone de Beauvoir, Paris. 

## Hershey ##
La famille MetaHershey est une traduction Metafont (paramétrique) des fontes de gravure Hershey, dont la particularité est d’être définies par leur squelette et non leur contour. La fonte est produite dans la tension entre ces deux différentes manières de concevoir la typographie.

## Reglo ##
Fonte déssinée par Sébastien Sébastien Sanfilippo et utilisée pour la communication de Radio Panik.


Online
------
## http://osp.kitchen ##
EN:
FR: le site web d’OSP diffuse en temps réel les fichiers de travail des membres d’OSP. Le site permet de visualiser chaque travail en cours et son processus et met en avant les questions de partage dans le design graphique. Les mises à jour sont accompagnées de messages descriptifs, requis par le logiciel de collaboration Git utilisé par OSP et dont le site découle.

## http://www.spion.me ##

FR: Site web pour un group de recherche académique vers la vie privée et la sécurité dans les réseaux sociaux. Le site reprends les données des statistiques collectionnés en temps réel et le rediffuse sur le page d’accueil.

## Oral Site http://sarma.be/oralsite ##

EN: Oral Site is an open source software and platform, commissioned by the dramaturgy collective Sarma. Oral Site allows for the creation of expanded publications, mixing text with images, video and sound recording and exploring new forms of temporality in text.
FR: Logiciel et plateforme libre, commisioné par le collectif des dramaturgues Sarma. Oral site permet de créer des publications mélangant textes, images, videos et enregistrements sonores, en explorant des nouvelles formes de temporalité textuelle.

Images:
Mis en page par le chorégraphe Julien Bruneau en utilisant Oral Site:
Strata (en développement)
Enchanting Scores